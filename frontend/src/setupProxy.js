const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = app => {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'http://prirazlomnaya-360.ru',
      changeOrigin: true,
    })
  )
  app.use(
    '/uploads',
    createProxyMiddleware({
      target: 'http://prirazlomnaya-360.ru',
      changeOrigin: true,
    })
  )
}
