import axios from 'axios'
import { LoadVideosResult } from './model'

export const loadVideos = async () => {
  const { data } = await axios.get<LoadVideosResult>('/api/get-video')
  return data
}
