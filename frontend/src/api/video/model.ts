export type LoadVideosResult = Array<{
  number: string
  name: string
  title: string
  text_kids: string | null
  text_main: string | null
  audio_kids: string
  audio_main: string
  is_internal: boolean
  url: string
  children: Array<{
    number: string
    name: string
    url: string
  }>
}>
