import axios from 'axios'
import { LoadLogoResult } from './model'

export const loadLogos = async () => {
  const { data } = await axios.get<LoadLogoResult>('/api/get-logo')
  return data
}
