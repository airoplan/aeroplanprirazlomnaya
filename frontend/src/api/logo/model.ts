export type LoadLogoResult = Array<{
  src_colored: string
  src_white: string
  url: string
  alt: string
}>
