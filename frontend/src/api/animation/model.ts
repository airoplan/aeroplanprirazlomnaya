export type LoadAnimationSlidesTextResult = Array<{
  slide_number: number
  text: string
  audio: string
}>
