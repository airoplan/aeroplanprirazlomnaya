import axios from 'axios'
import { LoadAnimationSlidesTextResult } from './model'

export const loadAnimationSlidesText = async () => {
  const { data } = await axios.get<LoadAnimationSlidesTextResult>('/api/get-slides')
  return data
}
