export type LoadAudioResult = {
  data: Array<{
    type: 4
    title: string
    url: string
  }>
  type_list: {
    [key: number]: string
  }
}
