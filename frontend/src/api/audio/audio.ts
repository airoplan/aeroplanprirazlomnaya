import axios from 'axios'
import { LoadAudioResult } from './model'

export const loadAudios = async () => {
  const { data } = await axios.get<LoadAudioResult>('/api/get-audio')
  return data
}
