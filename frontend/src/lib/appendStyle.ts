const appendStyle = (element: HTMLElement, style: string) => {
  const styleElement = document.createElement('style')
  styleElement.textContent = style
  element.appendChild(styleElement)
}

export default appendStyle
