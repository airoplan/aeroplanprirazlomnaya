const toMMSS = (seconds: number) => {
  const date = new Date(seconds * 1000)

  return date.toISOString().slice(14, 19).replace('00', '0')
}

export default toMMSS
