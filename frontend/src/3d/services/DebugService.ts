import {Inject, OnlyInstantiableByContainer, Singleton} from "typescript-ioc";

@Singleton
@OnlyInstantiableByContainer
export class DebugService {

    private data:{[key:string]:any} = [];
    private _info:Array<any> = [];
    private _warning:Array<any> = [];
    private _error:Array<any> = [];

    public foo:number;

    constructor() {
        setInterval(() => {
            if (this._info.length > 0 || this._warning.length > 0 || this._error.length > 0) {
                console.log("Log data:");
            }
            this._info.forEach((data:any, index:number) => {
                console.info(data);
            });
            this._info.length = 0;

            this._warning.forEach((data:any, index:number) => {
                console.warn(data);
            });
            this._warning.length = 0;

            this._error.forEach((data:any, index:number) => {
                console.error(data);
            });
            this._error.length = 0;

        }, 1500);
        setInterval(() => {
            Object.keys(this.data).map((indicator) => {
                document.getElementById(indicator).innerText = this.data[indicator];
            });
        }, 100);
    }

    public setIndicator(code: string, value: any) {
        this.data[code] = value;
    }

    public getIndicator(code: string) {
        return this.data[code];
    }

    public info (data:any) {
        this._info.push(data);
    }

    public warning (data:any) {
        this._warning.push(data);
    }

    public error (data:any) {
        this._error.push(data);
    }
}