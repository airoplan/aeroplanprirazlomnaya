import {
    AmbientLight, AnimationMixer,
    Camera,
    Clock,
    Color, CubeReflectionMapping, DirectionalLight,
    DoubleSide,
    EdgesGeometry,
    Group, Intersection,
    LinearEncoding, LinearFilter, LinearToneMapping,
    LineBasicMaterial,
    LineSegments, MathUtils,
    Mesh,
    MeshBasicMaterial, MeshStandardMaterial,
    Object3D,
    OrthographicCamera,
    PerspectiveCamera, Raycaster, RepeatWrapping,
    Scene, TextureLoader, Vector2, Vector3,
    WebGLRenderer, Wrapping
} from "three";
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer";
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass";
import { GLTF, GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { Object3DHelper } from "../helpers/Object3DHelper";
import { BleachBypassShader } from 'three/examples/jsm/shaders/BleachBypassShader';
import { ColorCorrectionShader } from 'three/examples/jsm/shaders/ColorCorrectionShader';
import { FXAAShader } from 'three/examples/jsm/shaders/FXAAShader';
import { GammaCorrectionShader } from 'three/examples/jsm/shaders/GammaCorrectionShader';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { ACESFilmicToneMapping, sRGBEncoding } from 'three/src/constants';
import { SceneService } from '../services/SceneService';

require('fpsmeter');


export class KidsModel {
    container: HTMLElement;
    renderer: WebGLRenderer;
    composer: EffectComposer;
    camera: Camera;
    scene: Scene;
    light: AmbientLight;
    light1: DirectionalLight;
    clock: Clock;
    stats: FPSMeter;
    controls: OrbitControls;
    mesh: Group;
    raycaster: Raycaster;
    raycasterPoint: Vector2;
    intersectedObject: Mesh;
    intersectedObjectMaterial: MeshBasicMaterial;
    intersectedObjectActiveMaterial: MeshBasicMaterial;
    mixer: AnimationMixer;
    points: Array<Mesh> = [];

    /**
     * Метод реализовывает вход в уровень
     */
    async enter(): Promise<void> {

        await this.beforeInit().then(async () => {
            await this.initScene();
            await this.initRenderer();
            this.initClock();
            this.initStats();
            this.initCamera();
            this.initLights();
            this.initPostprocessing();
            await this.initModel();
            await this.initRaycaster();
            this.initControls();
            this.addEventListeners();
        });

        return await this.afterInit();
    }

    protected async beforeInit() {

    }

    protected async initStats() {

        /*this.stats = new FPSMeter(
            document.getElementById('stats_container'),
            {
                theme: 'colorful',
                heat: 1,
            }
        );*/
    }

    protected async initClock() {
        this.clock = new Clock();
    }

    protected async initScene() {

        this.scene = new Scene();
        this.scene.background = null;
        //this.scene.fog = new Fog(0xE0FFFF, 100, 500);
    }

    protected async initCamera() {

        this.camera = new PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 4000)
    }

    protected async initRenderer() {

        this.renderer = new WebGLRenderer({ antialias: true, alpha: true, precision: 'highp' });
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.setClearColor(0x000000, 0);
        this.container = document.getElementById('gl_container');
        this.container.appendChild(this.renderer.domElement);
        this.renderer.shadowMap.enabled = true;
        this.renderer.outputEncoding = sRGBEncoding;
        this.renderer.shadowMap.type = 2;
        this.renderer.physicallyCorrectLights = true;
        this.renderer.toneMappingExposure = 1;
    }

    protected async initPostprocessing() {

        this.renderer.autoClear = false;

        const renderModel = new RenderPass(this.scene, this.camera);

        const effectBleach = new ShaderPass(BleachBypassShader);
        const effectColor = new ShaderPass(ColorCorrectionShader);
        let effectFXAA = new ShaderPass(FXAAShader);
        const gammaCorrection = new ShaderPass(GammaCorrectionShader);

        effectFXAA.uniforms['resolution'].value.set(1 / window.innerWidth, 1 / window.innerHeight);

        effectBleach.uniforms['opacity'].value = 0.1;

        effectColor.uniforms['powRGB'].value.set(1.4, 1.45, 1.45);
        effectColor.uniforms['mulRGB'].value.set(1.1, 1.1, 1.1);

        this.composer = new EffectComposer(this.renderer);

        this.composer.addPass(renderModel);
        this.composer.addPass(effectFXAA);
        this.composer.addPass(effectBleach);
        this.composer.addPass(effectColor);
        this.composer.addPass(gammaCorrection);
    }

    protected async initLights() {

        this.light = new AmbientLight(0xffffff, 2.5);
        this.scene.add(this.light);

        this.light1 = new DirectionalLight(0xffffff, 1);
        this.scene.add(this.light1);
    }

    protected async addEventListeners() {
        window.addEventListener('resize', this.onWindowResize.bind(this));
        // document.getElementById('gl_stats_container').addEventListener('click', this.onClick.bind(this));
        document.addEventListener('mousemove', this.onPointerMove.bind(this));
        document.addEventListener('click', this.onPointerMove.bind(this));
        document.addEventListener('touchmove', this.onPointerMove.bind(this));
        document.addEventListener('touchstart', this.onPointerMove.bind(this));
    }

    protected async initModel() {
        let loader = new GLTFLoader();
        const textureLoader = new TextureLoader();
        await loader.loadAsync('models/color.glb')
            .then(
                (gltf: GLTF) => {
                    console.log(gltf);

                    this.scene.add(gltf.scene);
                    this.mesh = gltf.scene;
                    this.mesh.scale.set(128, 128, 128);

                    this.mesh.traverse((object: Object3D) => {
                        if (object instanceof Mesh && object.name === 'mesh1') {
                            if (!Array.isArray(object.material)) {
                                //object.material.map.minFilter = LinearFilter;
                            }
                        }
                        if (object instanceof Mesh && object.name === 'mesh2') {
                            if (!Array.isArray(object.material)) {
                                //object.material.map.minFilter = LinearFilter;
                            }
                        }
                    });


                    const animations = gltf.animations;
                    this.mixer = new AnimationMixer(gltf.scene);
                    this.mixer.clipAction(animations[0]).play();
                },
                (reason: any) => {
                    console.error("Rejected: " + reason);
                }
            );
    }

    protected async initRaycaster() {
        this.raycaster = new Raycaster();
        this.raycasterPoint = new Vector2(10000, 10000);
        this.intersectedObjectActiveMaterial = new MeshBasicMaterial({
            color: new Color(0xff0000),
            transparent: true,
            opacity: 1,
            side: 2,
        });
        this.intersectedObjectMaterial = new MeshBasicMaterial({
            color: new Color(0xffffff),
            transparent: true,
            opacity: 1,
            side: 2,
        });
    }

    protected async initControls() {
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        //this.controls.listenToKeyEvents( window ); // optional

        //controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)

        this.controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        this.controls.dampingFactor = 0.025;

        this.controls.screenSpacePanning = false;
        this.controls.enablePan = false;

        this.controls.minDistance = 270;
        this.controls.maxDistance = 300;

        this.controls.minPolarAngle = Math.PI / 16;
        this.controls.maxPolarAngle = Math.PI / 2 - Math.PI / 8;
        this.controls.update();

        this.camera.position.set(-57.993768642680244, 175.13778190094818, -384.5892417671902);
        this.camera.rotation.set(-2.8834610984719182, -0.19453727825198508, -3.090598159431723);
    }

    /**
     * Метод реализовывает выход из уровня
     */
    async exit(): Promise<void> {
        const serv = new SceneService();
        serv.clearScene(this.scene);
    }

    /**
     * Метод будет вызван в самом конце входа в уровень
     */
    protected async afterInit(): Promise<void> {
        this.animate();
    }

    onWindowResize() {
        if (this.camera instanceof PerspectiveCamera || this.camera instanceof OrthographicCamera) {
            this.camera['aspect'] = window.innerWidth / window.innerHeight;
            this.camera.updateProjectionMatrix()
        }
        this.renderer.setSize(window.innerWidth, window.innerHeight)
    }

    onPointerMove(event) {
        this.raycasterPoint.x = (event.clientX / window.innerWidth) * 2 - 1;
        this.raycasterPoint.y = -(event.clientY / window.innerHeight) * 2 + 1;
        this.intersect()
    }

    onClick() {
        console.info('camera', this.camera);
        console.info('controls', this.controls);
        console.info('mesh', this.mesh);
    }

    animate(): void {
        //this.stats.tickStart();

        requestAnimationFrame(() => {
            this.animate()
        });
        this.controls.update();

        let delta = this.clock.getDelta();
        this.mixer.update(delta);

        const radAngle: number = this.controls.getAzimuthalAngle();
        const degAngle: number = MathUtils.radToDeg(radAngle) + 100;
        const element = document.getElementById('rotateCompas');
        if (element) {
            element.style.transform = 'rotate(' + degAngle + 'deg)';
        }

        this.render();
        //this.glstats();
        //this.stats.tick();
    }

    intersect(): void {

        if (this.intersectedObject) {
            this.uncolor(this.intersectedObject);
            this.intersectedObject = null;
        }

        this.raycaster.setFromCamera(this.raycasterPoint, this.camera);
        const intersects = this.raycaster.intersectObjects(this.scene.children, true);

        let isReturn = false;
        intersects.forEach((value: Intersection<Object3D<Event>>, index: number) => {
            if (isReturn) {
                return;
            }
            if (value.object instanceof Mesh && value.object.name.indexOf('_') === 0) {

                if (value.object) {
                    let number = value.object.name.replace('_', '').replace('box_', '');
                    this.scene.traverse((object: Object3D<Event>) => {
                        if (object instanceof Mesh && object.name.indexOf('_box_') === -1 && object.name.indexOf('_') === 0) {
                            const object_number = object.name.replace('_', '');
                            if (object_number === number) {
                                this.color(object);
                                this.intersectedObject = object;
                                isReturn = true;
                            } else {
                                this.uncolor(object);
                            }
                        }
                    });
                }

            }
        });
    }

    color(mesh: Mesh): void {
        mesh.traverse((object: Object3D) => {
            if (object instanceof LineSegments) {
                object.material = new LineBasicMaterial({ color: 0xED8B00, linewidth: 1 });
            }
        });
    }

    uncolor(mesh: Mesh): void {
        mesh.traverse((object: Object3D) => {
            if (object instanceof LineSegments) {
                object.material = new LineBasicMaterial({ color: 0x002855, linewidth: 1 });
            }
        });
    }

    glstats() {
        let text = 'calls ' + this.renderer.info.render.calls;
        text += ' | points ' + this.renderer.info.render.points;
        text += ' | triangles ' + this.renderer.info.render.triangles;
        text += ' | geometries ' + this.renderer.info.memory.geometries;
        text += ' | textures ' + this.renderer.info.memory.textures;
        text += ' | programs ' + this.renderer.info.programs.length;
        //document.getElementById('gl_stats_container').innerText = text;
    }

    render(): void {


        this.composer.render();

        //console.log(this.camera.position);
        //console.log(this.camera.rotation);

    }

}