import PrettyScrollbars from 'app/components/PrettyScrollbars/PrettyScrollbars'
import cn from 'classnames'
import classes from './SlideText.module.css'

type SlideTextProps = {
  className?: string
  text: string
}

const SlideText: React.FC<SlideTextProps> = ({ className, text }) => {
  return (
    <div
      className={cn(className, classes.text)}
      onWheel={e => e.stopPropagation()}
    >
      <PrettyScrollbars hide autoScroll>
        <div className={classes.text_inner}>{text}</div>
      </PrettyScrollbars>
    </div>
  )
}

export default SlideText
