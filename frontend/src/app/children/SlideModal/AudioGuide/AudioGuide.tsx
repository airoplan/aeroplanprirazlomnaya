import { FC } from 'react'
import toMMSS from 'lib/toMMSS'
import useAudioGuide from 'app/hooks/useAudioGuide'
import { ReactComponent as AudioWaveIcon } from 'icons/audio-wave.svg'
import { ReactComponent as PlayIcon } from './icons/play.svg'
import { ReactComponent as PauseIcon } from './icons/pause.svg'
import classes from './AudioGuide.module.css'

const options = {
  height: 4,
  progressColor: '#005CB9',
} as const

type AudioGuideProps = {
  src: string
}

const AudioGuide: FC<AudioGuideProps> = ({ src }) => {
  const {
    waveformContainerRef,
    isPlaying,
    isLoading,
    duration,
    progress,
    handlePlay,
    handlePause,
  } = useAudioGuide(src, options)

  return (
    <div className={classes.guide}>
      <div className={classes.guide_body}>
        <div ref={waveformContainerRef} className={classes.guide_timeline}>
          {isLoading && (
            <div className={classes.guide_timeline_loader}>
              <AudioWaveIcon />
            </div>
          )}
        </div>
        <button
          className={classes.control}
          disabled={isLoading}
          onClick={isPlaying ? handlePause : handlePlay}
        >
          {isPlaying ? <PauseIcon /> : <PlayIcon />}
        </button>
      </div>
      <div className={classes.guide_title}>
        Аудиогид{!isLoading && <span>. {toMMSS(duration - progress)}</span>}
      </div>
    </div>
  )
}

export default AudioGuide
