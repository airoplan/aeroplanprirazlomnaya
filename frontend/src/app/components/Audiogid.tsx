import classes from './Audiogid.module.css'
import { useEffect, useState } from 'react';

interface IProps {
    modal: boolean
}

export const Audiogid: React.FC<IProps> = ({ modal }) => {
    const secondScreen = (window.innerHeight)*0.95

    useEffect(() => {
        window.addEventListener('scroll', handleScroll, { passive: true });
  
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    const [scrollPosition, setScrollPosition] = useState(0);

    const handleScroll = () => {
        const position = window.pageYOffset;
        setScrollPosition(position);
    };
  
    return (
        <div className={ scrollPosition >= secondScreen || modal 
                ? classes.audiogid
                : [classes.audiogid, classes.invisible].join(' ')
            }>
            <div className={classes.description}
                style={ modal
                    ? {color: '#ffffff'}
                    : {color: '#F08300'}
                }
            >
                Виртуальный тур по платформе
            </div>
            <div className={classes.player_controls}>
                <div className={classes.player_timeline}></div>
                <div className={classes.player_play}></div>
            </div>
            <div className={classes.player_info}>
                <div className={classes.info_main}>
                    <div className={classes.player_title}
                        style={ modal
                            ? {color: '#ffffff'}
                            : {color: '#000000'}
                        }
                    >Аудиогид</div>
                    <div className={classes.description_group}>
                        <div className={classes.player_desc}
                            style={ modal
                                ? {color: '#ffffff'}
                                : {color: '#ababab'}
                            }
                        >Общий план
                        </div>
                        <div className={classes.info_time}
                            style={ modal
                                ? {color: '#ffffff'}
                                : {color: '#000000'}
                            }
                        >0:45</div>
                    </div>
                    
                </div>
            </div>
        </div>
    )
};