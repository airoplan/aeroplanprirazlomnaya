import React from 'react'
import { KidsModel } from '3d/base/KidsModel'

class PlatformKids extends React.Component<any, any> {
  private model: KidsModel
  render() {
    return (
      <div>
        <div id='gl_container'></div>
      </div>
    )
  }
  async componentDidMount() {
    this.model = new KidsModel()
    await this.model.enter()
  }

  async componentWillUnmount() {
    await this.model.exit()
  }
}

export default PlatformKids
