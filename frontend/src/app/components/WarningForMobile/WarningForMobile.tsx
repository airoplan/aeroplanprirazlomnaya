import { FC } from 'react'
import './WarningForMobile.css'

const WarningForMobile: FC = () => {
  return <div className='warning-for-mobile' />
}

export default WarningForMobile
