import { useEffect } from 'react'
import { Route, Routes } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { loadLogos } from 'app/redux/logo/actions'
import { loadAnimationSlidesText } from 'app/redux/animation/actions'
import { loadAudios } from 'app/redux/audio/actions'
import { loadSlider } from 'app/redux/slider/actions'
import WarningForMobile from 'app/components/WarningForMobile/WarningForMobile'
import Home from './home/Home'
import Adult from './adult/Adult'
import Children from './children/Children'
import { NotFound } from './404/NotFound'

const App = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(loadLogos())
    dispatch(loadAnimationSlidesText())
    dispatch(loadAudios())
    dispatch(loadSlider())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      <WarningForMobile />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='adult' element={<Adult />} />
        <Route path='children' element={<Children />} />
        <Route path='*' element={<NotFound />} />
      </Routes>
    </>
  )
}

export default App
