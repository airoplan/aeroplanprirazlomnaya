import { FC } from 'react'
import toMMSS from 'lib/toMMSS'
import useAudioGuide from 'app/hooks/useAudioGuide'
import { ReactComponent as AudioWaveIcon } from 'icons/audio-wave.svg'
import classes from './AudioGuide.module.css'

const options = {
  progressColor: '#F08300',
} as const

type AudioGuideProps = {
  mode: 'colored' | 'white'
  src: string
}

const AudioGuide: FC<AudioGuideProps> = ({ mode, src }) => {
  const {
    waveformContainerRef,
    isPlaying,
    isLoading,
    duration,
    progress,
    handlePlay,
    handlePause,
  } = useAudioGuide(src, options)

  return (
    <div className={classes.guide}>
      <div className={classes.guide_controlls}>
        <div ref={waveformContainerRef} className={classes.guide_timeline}>
          {isLoading && (
            <div className={classes.guide_timeline_loader}>
              <AudioWaveIcon
                style={{ color: mode === 'white' ? '#fff' : '#F08300' }}
              />
            </div>
          )}
        </div>
        <button
          className={isPlaying ? classes.stop_btn : classes.play_btn}
          disabled={isLoading}
          onClick={isPlaying ? handlePause : handlePlay}
        />
        <div className={classes.info_main}>
          <div
            className={classes.player_title}
            style={
              mode === 'white' ? { color: '#ffffff' } : { color: '#000000' }
            }
          >
            Аудиогид
          </div>
          <div className={classes.description_group}>
            {!isLoading && (
              <div
                className={classes.info_time}
                style={
                  mode === 'white' ? { color: '#ffffff' } : { color: '#000000' }
                }
              >
                {toMMSS(duration - progress)}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default AudioGuide
