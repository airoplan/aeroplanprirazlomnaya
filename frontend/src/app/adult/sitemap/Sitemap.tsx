import { FC } from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { sortedSlidesSelector } from 'app/redux/slider/selectors'
import classes from './Sitemap.module.css'

type SitemapProps = {
  onSelect: (id: string) => void
}

export const Sitemap: FC<SitemapProps> = ({ onSelect }) => {
  const slides = useSelector(sortedSlidesSelector)


  const handleReload = () => {
    window.location.reload()
  }

  return (
    <div className={classes.sitemap_page}>
      <div className={classes.sitemap_content}>
        <h2 className={classes.sitemap_title}>Карта сайта</h2>
        <div className={classes.slides}>
          {slides.map(({ id, title }) => (
            <button
              key={id}
              className={classes.slide_item}
              onClick={() => onSelect(id)}
            >
              {title}
            </button>
          ))}
          <Link className={classes.nav_item} to='/adult' onClick={handleReload}>
            Вернуться на главную
          </Link>
          <Link className={classes.nav_item} to='/children'>
            Взглянуть иначе
          </Link>
        </div>
      </div>
    </div>
  )
}
