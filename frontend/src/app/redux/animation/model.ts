type AnimationState = {
  isLoading: boolean
  error: Error | null
  slides: Array<Slide>
}

type Slide = {
  slideNumber: number
  text: string
  audio: string
}

export type { AnimationState, Slide }
