import { RootState } from '../store'

export const animationSlidesSelector = (state: RootState) => {
  return state.animation.slides
}
