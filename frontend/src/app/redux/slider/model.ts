type SliderState = {
  isLoading: boolean
  error: Error | null
  data: Array<SliderItem>
}

type SliderItem = {
  id: string
  name: string
  title: string
  text_kids: string | null
  text_main: string | null
  audio_kids: string
  audio_main: string
  url: string
  is_internal: boolean
  active: boolean
  children: Array<{
    id: string
    name: string
  }>
}

export type { SliderState, SliderItem }
