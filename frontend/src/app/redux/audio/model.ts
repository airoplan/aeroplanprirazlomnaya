type AudiosState = {
  isLoading: boolean
  error: Error | null
  data: Array<Audio>
  types: AudioTypes
}

type Audio = {
  type: number
  title: string
  url: string
}

type AudioTypes = {
  [key: number]: string
}

export type { AudiosState, Audio, AudioTypes }
