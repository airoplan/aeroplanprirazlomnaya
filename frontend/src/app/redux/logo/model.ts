type AudiosState = {
  isLoading: boolean
  error: Error | null
  data: Array<Logo>
}

type Logo = {
  src_colored: string
  src_white: string
  url: string
  alt: string
}

export type { AudiosState, Logo }
