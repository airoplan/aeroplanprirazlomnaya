import { RootState } from '../store'

export const logosSelector = (state: RootState) => state.logo.data
