import { KeyboardEvent, useEffect } from 'react'

const useKeyDown = (keyCode: string, onKeyDown: () => void) => {
  useEffect(() => {
    const handleKeyDown = ({ code }: KeyboardEvent) => {
      if (code !== keyCode) {
        return
      }

      onKeyDown()
    }

    // @ts-expect-error bad typing
    window.addEventListener('keydown', handleKeyDown)

    return () => {
      // @ts-expect-error bad typing
      window.removeEventListener('keydown', handleKeyDown)
    }
  }, [keyCode])
}

export default useKeyDown
