<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\User */

$this->title = Yii::t('app', 'Добавление пользователя');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователи'), 'url' => ['index']];
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>

