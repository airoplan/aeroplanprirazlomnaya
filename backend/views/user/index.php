<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel \app\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Пользователи');
$this->params['breadcrumbs'][] = $this->title;
?>

<p>
    В этом разделе Вы можете управлять пользователями панели управления.
</p>

<p>
    <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-primary']) ?>
</p>

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'layout' => '{pager}{summary}<div class="table-responsive">{items}</div>',
    'columns' => [
        'username',
        'role:text:Роль',
        'email:email',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}',
        ],
    ],
]); ?>



