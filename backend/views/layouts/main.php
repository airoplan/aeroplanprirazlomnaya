<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= $this->title ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    $navItems = [
        [
            'label' => 'Менеджмент контента',
            'items' => [
                [
                    'label' => 'Видео 360&deg; / Панорамных фото',
                    'url' => ['/video/index'],
                ],
                [
                    'label' => 'Аудиофайлы',
                    'url' => ['/audio/index'],
                ],
                [
                    'label' => 'Логотипы',
                    'url' => ['/logo/index'],
                ]
            ],
        ],
        [
            'label' => 'Перейти на сайт',
            'url' => Yii::$app->params['frontUrl'] ?? null,
        ],
    ];
    if (Yii::$app->user->can(\app\helpers\PermissionHelper::ROLE_ADMIN)) {
        $navItems[] =
            [
                'label' => 'Пользователи',
                'url' => ['/user'],
            ];
    }
    $navItems[] = Yii::$app->user->isGuest ? (
    ['label' => 'Войти', 'url' => ['/site/login']]
    ) : (
        '<li>'
        . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
        . Html::submitButton(
            'Выход',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>'
    );

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'encodeLabels' => false,
        'items' => $navItems,
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'homeLink' => [
                'label' => 'Back-end',
                'url' => '/',
            ],
            'encodeLabels' => false,
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; Студия графического дизайна Аэроплан. <?= date('Y') ?></p>
        <p class="float-right"><?= \yii\helpers\Html::a('airoplan.ru', 'https://airoplan.ru/') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
