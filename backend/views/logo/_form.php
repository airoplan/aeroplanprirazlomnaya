<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\LogoForm */
/* @var $form yii\widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data'
    ],
]); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'alt')->textInput() ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'url')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'file')->fileInput(); ?>
            <?php if ($model->img && $url = \app\helpers\UploadVideoHelper::getUsefulUrlFromLogo($model)): ?>
                <?= Html::img($url, [
                    'style' => 'width:200px;',
                ]); ?>
            <?php endif; ?>
            <hr>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'file1')->fileInput(); ?>
            <?php if ($model->img1 && $url = \app\helpers\UploadVideoHelper::getUsefulUrlFromLogo1($model)): ?>
                <?= Html::img($url, [
                    'style' => 'width:200px;',
                ]); ?>
            <?php endif; ?>
            <hr>
        </div>
        <div class="col-md-12">
            <hr>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', [
                    'class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary'
                ]) ?>
            </div>

        </div>
    </div>

<?php ActiveForm::end(); ?>