<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Логотипы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logo-index">

    <h1><?= $this->title ?></h1>

    <p>
        В этом разделе Вы можете загружать логотипы.
    </p>

    <p>
        <?= Html::a('Добавить логотип', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => 'Пока нет загруженных логотипов. Нажмите на кнопку <i>Добавить логотип</i> чтобы загрузить новый логотип.',
        'columns' => [
            'alt',
            'url',
            [
                'attribute' => 'img',
                'label' => 'Логотип',
                'format' => 'raw',
                'value' => function (\app\models\Logo $logo) {
                    if ($logo->img) {
                        return \yii\bootstrap4\Html::img(\app\helpers\UploadVideoHelper::getUsefulUrlFromLogo($logo), [
                            'style' => 'width:100px;',
                        ]);
                    }
                }
            ],
            [
                'attribute' => 'img1',
                'label' => 'Логотип (другой цвет)',
                'format' => 'raw',
                'value' => function (\app\models\Logo $logo) {
                    if ($logo->img1) {
                        return \yii\bootstrap4\Html::img(\app\helpers\UploadVideoHelper::getUsefulUrlFromLogo1($logo), [
                            'style' => 'width:100px;',
                        ]);
                    }
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]); ?>

</div>
