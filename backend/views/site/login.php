<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = 'Вход в панель управления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Пожалуйста, заполните поля для входа:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 col-form-label'],
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'password')->passwordInput() ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-1">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
                <div class="col-lg-3" style="text-align: right; ">
                    <?= Html::a('Забыли пароль?', ['site/request-password-reset'], [
                        'style' => 'margin-top: 5px;display: block;margin-right: 15px;'
                    ]) ?>
                </div>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>
</div>
