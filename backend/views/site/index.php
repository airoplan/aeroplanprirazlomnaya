<?php

/* @var $this yii\web\View */

$this->title = 'Панель управления';


?>

<div class="site-index align-middle">
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Добро пожаловать в панель управления!</h1>

        <p class="lead">Воспользуйтесь ссылками в верхнем меню для навигации или перейдите на сайт
            разработчика</p>

        <p><a class="btn btn-md btn-primary" href="https://airoplan.ru">airoplan.ru</a></p>
    </div>
</div>

