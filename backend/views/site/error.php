<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Если Вы видите эту страницу — значит возникла техническая ошибка.
    </p>
    <p>
        Мы были бы признательны если Вы сделаете скриншот экрана и пришлёте его нам
    </p>

</div>