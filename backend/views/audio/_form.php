<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\AudioForm */
/* @var $form yii\widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data'
    ],
]); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'type')->widget(\kartik\select2\Select2::class, [
                'data' => \app\models\Audio::getList(),
                'options' => [
                    'prompt' => 'Выберите предназначение ... ',
                ],
            ])->label('Предназначение') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'file')->fileInput(); ?>
            <?php if ($model->upload_path && $url = \app\helpers\UploadVideoHelper::getUsefulUrlFromAudioBase($model)): ?>
                <?= Html::a('Нажмите на эту ссылку чтобы посмотреть видео', \yii\helpers\Url::to($url), ['target' => '_blank']); ?>
            <?php endif; ?>
            <hr>
        </div>
        <div class="col-md-12">
            <hr>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', [
                    'class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary'
                ]) ?>
            </div>

        </div>
    </div>

<?php ActiveForm::end(); ?>