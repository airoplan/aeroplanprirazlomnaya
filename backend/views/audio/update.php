<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Audio */

$this->title = 'Редактирование аудиофайла';
$this->params['breadcrumbs'][] = ['label' => 'Аудиофайлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование аудиофайла';
?>
<div class="video-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
