<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Audio */

$this->title = 'Добавление аудиофайла';
$this->params['breadcrumbs'][] = ['label' => 'Аудиофайлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
