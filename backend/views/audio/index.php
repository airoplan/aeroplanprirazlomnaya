<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AudioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Аудиофайлы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audio-index">

    <h1><?= $this->title ?></h1>

    <p>
        В этом разделе Вы можете загружать аудиофайлы. Поддерживается формат MP3.
    </p>

    <p>
        <?= Html::a('Добавить аудио', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => 'Пока нет загруженных аудио. Нажмите на кнопку <i>Добавить аудио</i> чтобы загрузить новый аудиофайл.',
        'columns' => [
            'title',
            [
                'attribute' => 'type',
                'value' => function(\app\models\AudioForm $model) {
                    return \app\models\Audio::getList()[$model->type] ?? 'Не указано';
                }
            ],
            [
                'attribute' => 'upload_path',
                'label' => 'Ссылка на аудио',
                'format' => 'raw',
                'value' => function (\app\models\Audio $audio) {
                    $url = \app\helpers\UploadVideoHelper::getUsefulUrlFromAudioBase($audio);
                    if (!$audio->upload_path) {
                        return 'Аудио ещё не было загружено';
                    }
                    return Html::a('Нажмите, чтобы послушать', \yii\helpers\Url::to($url));
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]); ?>

</div>
