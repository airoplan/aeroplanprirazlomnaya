<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Video */

$this->title = 'Редактирование видео';
$this->params['breadcrumbs'][] = ['label' => 'Видео 360° / Панорамные фото', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование видео';
?>
<div class="video-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
