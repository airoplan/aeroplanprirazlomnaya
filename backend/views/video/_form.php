<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\VideoForm */
/* @var $form yii\widgets\ActiveForm */

if (is_array($model->numbers)) {
    $numbers = array_values($model->numbers);
}

?>

<?= \yii\bootstrap4\Accordion::widget([
    'items' => [
        [
            'label' => 'Подсказки',
            'content' => '
                    Для загрузки видео выберите <b>zip</b> архив с <b>результатом экспорта под web из программы 3DVista Virtual
            Tour</b>.
    <br>Поле для ввода <i>Индекс мультимедиа</i> это порядковый номер видео в проекте 3D Vista. <a href="https://www.3dvista.com/en/blog/the-url-of-a-virtual-tour/">Подробнее про <b>media-index</b> и другие URL параметры </a>
    <br>Поле для ввода <i>Номер</i> будет использоваться для связки по номерам к точкам.
    <br>Поле для ввода <i>Наименование</i> будет использоваться для отображения наименования.
    </p>
                    ',
        ],
        [
            'label' => 'Как экспортировать под web в программе 3DVista Virtual Tour',
            'content' => '<img src="/images/84ce60381b.jpg" style="width: 100%;">',
        ],
    ],
    'autoCloseItems' => true
]); ?>

    <hr>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data'
    ],
]); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'numbers')->widget(MultipleInput::class, [
                'data' => $numbers,
                'max' => 255,
                'min' => 1,
                'allowEmptyList' => false,
                'enableGuessTitle' => true,
                'addButtonPosition' => MultipleInput::POS_HEADER,
                'columns' => [
                    [
                        'name' => 'media-index',
                        'type' => 'textinput',
                        'title' => 'Индекс мультимедиа (playlist index)',
                        'options' => [
                            'class' => 'input-callback'
                        ]
                    ],
                    [
                        'name' => 'video-number',
                        'type' => 'textinput',
                        'title' => 'Номер видео',
                        'options' => [
                            'class' => 'input-callback'
                        ]
                    ],
                    [
                        'name' => 'name',
                        'type' => 'textinput',
                        'title' => 'Наименование',
                        'options' => [
                            'class' => 'input-callback'
                        ]
                    ]
                ],
            ])->label(false); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput() ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'active')->checkbox()->label(false) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'is_internal')->checkbox()->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'file')->fileInput(); ?>
            <?php if ($model->upload_path && $url = \app\helpers\UploadVideoHelper::getUsefulUrlFromVideo($model)): ?>
                <?= Html::a('Нажмите на эту ссылку чтобы посмотреть видео', \yii\helpers\Url::to($url), ['target' => '_blank']); ?>
            <?php endif; ?>
            <hr>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'file_audio_main')->fileInput(); ?>
            <?php if ($model->audio_main && $url = \app\helpers\UploadVideoHelper::getUsefulUrlFromAudioMain($model)): ?>
                <?= Html::a('Нажмите на эту ссылку чтобы прослушать аудио', \yii\helpers\Url::to($url), ['target' => '_blank']); ?>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'file_audio_kids')->fileInput(); ?>
            <?php if ($model->audio_kids && $url = \app\helpers\UploadVideoHelper::getUsefulUrlFromAudioKids($model)): ?>
                <?= Html::a('Нажмите на эту ссылку чтобы прослушать аудио', \yii\helpers\Url::to($url), ['target' => '_blank']); ?>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'text_main')->textarea(['rows' => 12]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'text_kids')->textarea(['rows' => 12]) ?>
        </div>
        <div class="col-md-12">
            <hr>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', [
                    'class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary'
                ]) ?>
            </div>

        </div>
    </div>

<?php ActiveForm::end(); ?>