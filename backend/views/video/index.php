<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Менеджмент Видео 360&deg; / Панорамных фото';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-index">

    <h1><?= $this->title ?></h1>

    <p>
        В этом разделе Вы можете загружать Видео 360&deg; из программы 3DVista, планируется добавить поддержку загрузки
        панорамных фото.
        <br>Нажмите на кнопку <i>Добавить видео</i> чтобы загрузить новое. В таблице ниже расположен список уже
        загруженных видео.
    </p>

    <p>
        <?= Html::a('Добавить видео', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => 'Пока нет загруженных видео. Нажмите на кнопку <i>Добавить видео</i> чтобы загрузить новое.',
        'columns' => [
            'title',
            [
                'attribute' => 'upload_path',
                'label' => 'Ссылка на видео',
                'format' => 'raw',
                'value' => function (\app\models\Video $video) {
                    $url = \app\helpers\UploadVideoHelper::getUsefulUrlFromVideo($video);
                    if (!$video->upload_path) {
                        return 'Видео ещё не было загружено';
                    }
                    return Html::a('Нажмите, чтобы посмотреть', \yii\helpers\Url::to($url));
                }
            ],
            'active:boolean',
            [
                'attribute' => 'audio_main',
                'label' => 'Аудиогид',
                'value' => function (\app\models\Video $model) {
                    return $model->audio_main ? 'Да' : 'Нет';
                }
            ],
            [
                'attribute' => 'audio_kids',
                'label' => 'Аудиогид детский',
                'value' => function (\app\models\Video $model) {
                    return $model->audio_kids ? 'Да' : 'Нет';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
        'afterRow' => function (\app\models\Video $video) {
            return '<tr><td colspan="6">' . GridView::widget([
                    'dataProvider' => new \yii\data\ArrayDataProvider(['models' => $video->numbers, 'key' => 'media-index']),
                    'emptyText' => 'Номера видео не указаны',
                    'summary' => false,
                    'columns' => [
                        [
                            'attribute' => 'media-index',
                            'label' => 'Индекс мультимедиа (playlist index)',
                        ],
                        [
                            'attribute' => 'video-number',
                            'label' => 'Номер видео',
                        ],
                        [
                            'attribute' => 'name',
                            'label' => 'Наименование',
                            'value' => function (array $data) use ($video) {
                                $url = \app\helpers\UploadVideoHelper::getUsefulUrlFromVideo($video);
                                if (!$video->upload_path) {
                                    return $data['name'];
                                }
                                return Html::a($data['name'], \yii\helpers\Url::to($url) . 'index.htm?media-index=' . $data['media-index']);
                            },
                            'format' => 'raw',
                        ],
                    ],
                ]) . '</td></tr>';
        }
    ]); ?>

</div>
