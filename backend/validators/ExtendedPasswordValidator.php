<?php
namespace app\validators;

use yii\validators\Validator;

class ExtendedPasswordValidator extends Validator
{

    public function validateAttribute($model, $attribute)
    {
        if (!preg_match('/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}/i', $model->$attribute)) {
            $this->addError($model, $attribute, 'Пароль должен содержать не менее 8 символов, буквы разного регистра, минимум 1 цифру и 1 спец. символ');
        }
    }

}