<?php
/**
 * Created by PhpStorm.
 * Author: Raidkon
 */

namespace app\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * Поведение для работы с полями типа text с json для малтипл полей
 *
 * @package app\behaviors
 * @property-read ActiveRecord $owner
 */
class ArrayAttributeBehavior extends Behavior
{
    public $attribute;

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'toDb',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'toDb',
            ActiveRecord::EVENT_AFTER_FIND => 'fromDb',
            ActiveRecord::EVENT_AFTER_REFRESH => 'fromDb',
            ActiveRecord::EVENT_INIT => 'fromDb',
            ActiveRecord::EVENT_AFTER_INSERT => 'fromDb',
            ActiveRecord::EVENT_AFTER_UPDATE => 'fromDb',
        ];
    }

    /**
     * Из массива в формат БД
     */
    public function toDb()
    {
        $attribute = $this->attribute;
        try {
            $this->owner->setAttribute($attribute, Json::encode($this->owner->$attribute ?? []));
        } catch (\Throwable $throwable) {
            $this->owner->setAttribute($attribute, '');
        }
    }

    /**
     * Из формата БД в массив для полей
     */
    public function fromDb()
    {
        $attribute = $this->attribute;
        try {
            $this->owner->$attribute = json_decode($this->owner->getAttribute($attribute), true, 512, JSON_UNESCAPED_UNICODE);
        }
        catch (\Throwable $throwable) {
            $this->owner->$attribute = [];
        }
    }

}
