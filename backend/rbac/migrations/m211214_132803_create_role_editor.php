<?php

use yii2mod\rbac\migrations\Migration;
use app\helpers\PermissionHelper;

class m211214_132803_create_role_editor extends Migration
{
    public function safeUp()
    {
        $this->createRole(PermissionHelper::ROLE_EDITOR);
    }

    public function safeDown()
    {
        $this->removeRole(PermissionHelper::ROLE_EDITOR);
    }
}