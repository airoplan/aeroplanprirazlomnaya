<?php

use yii2mod\rbac\migrations\Migration;
use app\helpers\PermissionHelper;

class m211214_132408_create_role_admin extends Migration
{
    public function safeUp()
    {
        $this->createRole(PermissionHelper::ROLE_ADMIN);
    }

    public function safeDown()
    {
        $this->removeRole(PermissionHelper::ROLE_ADMIN);
    }
}
 