<?php
/**
 * Created by PhpStorm.
 * User: kuftyrev
 * Date: 28.10.2021
 * Time: 21:38
 */

namespace app\models;
use app\models\User;
use app\validators\ExtendedPasswordValidator;
use Yii;
use yii\base\Model;
use yii2mod\user\models\enums\UserStatus;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class SignupForm extends Model
{
    const SCENARIO_CREATE = 'scenario-create';
    const SCENARIO_UPDATE = 'scenario-update';

    public $id;
    public $username;
    public $email;
    public $password;
    public $rememberMe = true;

    public $role;

    public $user = false;

    public function __construct(User $user = null, $config = [])
    {
        if ($user) {
            $this->user = $user;
            $this->role = $user->role;
        }

        parent::__construct($config);
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['username', 'email', 'role', 'password'],
            self::SCENARIO_CREATE => ['username', 'email', 'role', 'password'],
            self::SCENARIO_UPDATE => ['username', 'email', 'role', 'password'],
        ];
    }


    public function rules()
    {
        return [
            ['id', 'integer'],
            ['username', 'trim'],
            ['username', 'required', 'message' => 'Пожалуйста, укажите имя пользователя'],
            ['role', 'required', 'message' => 'Пожалуйста, укажите роль'],
            ['username', 'unique', 'targetClass' => User::class, 'message' => 'Пользователь с таким именем уже существует', 'on' => self::SCENARIO_CREATE],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required', 'on' => self::SCENARIO_CREATE, 'message' => 'Пожалуйста, укажите E-mail'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::class, 'message' => 'Пользователь с таким E-mail уже существует', 'on' => self::SCENARIO_CREATE ],

            ['password', 'required', 'on' => self::SCENARIO_CREATE, 'message' => 'Пожалуйста, укажите пароль'],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
            ['password', ExtendedPasswordValidator::class],
            [['role'], 'string'],
            ['rememberMe', 'boolean'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        } else {
            var_dump($this->errors);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->user === false) {
            $this->user = User::findByUsername($this->username);
        }

        return $this->user;
    }
    public function signup(): ?User
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status = UserStatus::ACTIVE;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($user->save() /* && $this->sendEmail($user)*/) {
            $user->role = $this->role;
            return $user;
        } else {
            var_dump($user->errors);
        }
        return null;
    }

    public function update(): ?User
    {
        if (!$this->validate()) {
            return null;
        }

        $this->user->username = $this->username;
        $this->user->email = $this->email;
        $this->user->status = UserStatus::ACTIVE;

        if ($this->password) {
            $this->user->setPassword($this->password);
        }

        $this->user->generateAuthKey();

        if ($this->user->save() /* && $this->sendEmail($user)*/) {
            $this->user->role = $this->role;
            return $this->user;
        }
        return null;
    }

    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
