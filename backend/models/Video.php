<?php

namespace app\models;

use app\behaviors\ArrayAttributeBehavior;

/**
 * This is the model class for table "video".
 *
 * @property int $id
 * @property int|null $active Отображать на модели
 * @property string|null $upload_path
 * @property string|null $title
 * @property string|null $audio_main
 * @property string|null $audio_kids
 * @property string|null $text_main
 * @property string|null $text_kids
 * @property boolean $is_internal
 */
class Video extends \yii\db\ActiveRecord
{
    public array $numbers = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['numbers'], 'safe'],
            [['is_internal'], 'boolean'],
            [['upload_path', 'title', 'audio_main', 'audio_kids'], 'string', 'max' => 255],
            [['text_main', 'text_kids'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Опубликовано',
            'number' => 'Номера видео',
            'title' => 'Наименование',
            'upload_path' => 'Upload Path',
            'text_main' => 'Текст к взрослой версии',
            'text_kids' => 'Текст к детской версии',
            'is_internal' => 'Помещение внутреннее',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\queries\VideoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\queries\VideoQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => ArrayAttributeBehavior::class,
                'attribute' => 'numbers',
            ],
        ];
    }


}
