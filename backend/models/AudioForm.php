<?php

namespace app\models;

use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use Yii;

class AudioForm extends Audio
{
    public ?string $file = null;
    public bool $delete_file = false;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['file'], 'file', 'maxSize' => 1024 * 1024 * 1024, 'extensions' => 'mp3', 'message' => 'Извините, но загружать можно только mp3'],
            [['delete_file'], 'boolean'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'file' => 'Выберите аудио файл на компьютере',
        ]);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->upload()) {
            return parent::save($runValidation, $attributeNames);
        }

        return false;
    }

    private function upload(): bool
    {
        $this->uploadAudio();

        return true;
    }

    private function uploadAudio(): void
    {
        $file = UploadedFile::getInstance($this, 'file');

        if ($file) {
            $basePath = Yii::getAlias('@uploadPath/audio_main/');
            $fileName = md5(Yii::$app->security->generateRandomString(8)) . '.' . $file->extension;
            if (!file_exists($basePath)) {
                FileHelper::createDirectory($basePath, 7777);
            }
            $file->saveAs($basePath . $fileName);

            $this->upload_path = $fileName;
        }

        if ($this->delete_file) {
            $this->upload_path = null;
        }
    }
}