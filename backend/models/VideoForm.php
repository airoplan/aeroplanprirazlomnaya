<?php


namespace app\models;


use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use Yii;

class VideoForm extends Video
{
    public ?string $file = null;
    public bool $delete_file = false;

    public ?string $file_audio_main = null;
    public bool $delete_file_audio_main = false;

    public ?string $file_audio_kids = null;
    public bool $delete_file_audio_kids = false;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['file'], 'file', 'maxSize' => 1024 * 1024 * 1024, 'extensions' => 'zip', 'message' => 'Извините, но загружать можно только архивы ZIP'],
            [['file_audio_main'], 'file', 'maxSize' => 1024 * 1024 * 1024, 'extensions' => 'mp3', 'message' => 'Извините, но загружать можно только mp3'],
            [['file_audio_kids'], 'file', 'maxSize' => 1024 * 1024 * 1024, 'extensions' => 'mp3', 'message' => 'Извините, но загружать можно только mp3'],
            [['delete_file'], 'boolean'],
            [['delete_file_audio_main'], 'boolean'],
            [['delete_file_audio_kids'], 'boolean'],
            [['title'], 'string', 'max' => 255],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'file' => 'Выберите zip архив с видео на компьютере',
            'file_audio_main' => 'Выберите взрослый аудио файл на компьютере',
            'file_audio_kids' => 'Выберите детский аудио файл на компьютере',
        ]);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->upload()) {
            return parent::save($runValidation, $attributeNames);
        }

        return false;
    }

    private function upload(): bool
    {
        $this->uploadVideo();
        $this->uploadAudioMain();
        $this->uploadAudioKids();

        return true;
    }

    private function uploadVideo(): void
    {
        $file = UploadedFile::getInstance($this, 'file');

        if ($file) {
            $basePath = Yii::getAlias('@uploadPath/video360/');
            $fileName = md5(Yii::$app->security->generateRandomString(8)) . '.' . $file->extension;
            if (!file_exists($basePath)) {
                FileHelper::createDirectory($basePath, 7777);
            }
            $file->saveAs($basePath . $fileName);

            $zip = new \ZipArchive();
            $zip->open($basePath . $fileName);
            $zip->extractTo($basePath . 'view/' . $fileName . '/');

            $this->upload_path = $fileName;
        }

        if ($this->delete_file) {
            $this->upload_path = null;
        }
    }

    private function uploadAudioMain(): void
    {
        $file = UploadedFile::getInstance($this, 'file_audio_main');

        if ($file) {
            $basePath = Yii::getAlias('@uploadPath/audio_main/');
            $fileName = md5(Yii::$app->security->generateRandomString(8)) . '.' . $file->extension;
            if (!file_exists($basePath)) {
                FileHelper::createDirectory($basePath, 7777);
            }
            $file->saveAs($basePath . $fileName);

            $this->audio_main = $fileName;
        }

        if ($this->delete_file_audio_main) {
            $this->audio_main = null;
        }
    }

    private function uploadAudioKids(): void
    {
        $file = UploadedFile::getInstance($this, 'file_audio_kids');

        if ($file) {
            $basePath = Yii::getAlias('@uploadPath/audio_kids/');
            $fileName = md5(Yii::$app->security->generateRandomString(8)) . '.' . $file->extension;
            if (!file_exists($basePath)) {
                FileHelper::createDirectory($basePath, 7777);
            }
            $file->saveAs($basePath . $fileName);

            $this->audio_kids = $fileName;
        }

        if ($this->delete_file_audio_kids) {
            $this->audio_kids = null;
        }
    }

}