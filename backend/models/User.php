<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use yii2mod\rbac\models\AssignmentModel;
use yii2mod\user\models\enums\UserStatus;
use yii2mod\user\models\UserModel;

/**
 * @property string $role
 */
class User extends UserModel
{
    const STATUS_ACTIVE   = 100;
    const STATUS_INACTIVE = 0;


    public function getRole()
    {
        $assignItems = (new AssignmentModel($this))->getItems();
        $roles = array_keys(ArrayHelper::getValue($assignItems, 'assigned'));
        return ArrayHelper::getValue($roles, 0);
    }

    public function setRole($role)
    {
        $assigmentModel = new AssignmentModel($this);
        \Yii::$app->authManager->revokeAll($this->id);
        $assigmentModel->assign([$role]);
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE   => 'Активный',
            self::STATUS_INACTIVE => 'Удален',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required', 'message' => 'Пожалуйста, укажите E-mail'],
            [['username',], 'required', 'message' => 'Пожалуйста, укажите имя пользователя'],
            ['email', 'unique', 'message' => 'Пользователь с таким E-mail уже существует'],
            ['username', 'unique', 'message' => 'Пользователь с таким именем уже существует'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['plainPassword', 'string', 'min' => 6],
            ['plainPassword', 'required', 'on' => 'create', 'message' => 'Пожалуйста, укажите пароль'],
            ['status', 'default', 'value' => UserStatus::ACTIVE],
            ['status', 'in', 'range' => UserStatus::getConstantsByName()],
        ];
    }
}
