<?php

namespace app\models;

use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use Yii;

class LogoForm extends Logo
{
    public ?string $file = null;
    public bool $delete_file = false;

    public ?string $file1 = null;
    public bool $delete_file1 = false;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['file'], 'file', 'maxSize' => 1024 * 1024 * 1024, 'extensions' => 'jpg,jpeg,png,gif,svg', 'message' => 'Извините, но загружать можно только изображения'],
            [['delete_file'], 'boolean'],
            [['file1'], 'file', 'maxSize' => 1024 * 1024 * 1024, 'extensions' => 'jpg,jpeg,png,gif,svg', 'message' => 'Извините, но загружать можно только изображения'],
            [['delete_file1'], 'boolean'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'file' => 'Выберите логотип (цветной) на компьютере',
            'file1' => 'Выберите логотип (белый) на компьютере',
        ]);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->upload()) {
            return parent::save($runValidation, $attributeNames);
        }

        return false;
    }

    private function upload(): bool
    {
        $this->uploadLogo();
        $this->uploadLogo1();

        return true;
    }

    private function uploadLogo(): void
    {
        $file = UploadedFile::getInstance($this, 'file');

        if ($file) {
            $basePath = Yii::getAlias('@uploadPath/logo/');
            $fileName = md5(Yii::$app->security->generateRandomString(8)) . '.' . $file->extension;
            if (!file_exists($basePath)) {
                FileHelper::createDirectory($basePath, 7777);
            }
            $file->saveAs($basePath . $fileName);

            $this->img = $fileName;
        }

        if ($this->delete_file) {
            $this->img = null;
        }
    }

    private function uploadLogo1(): void
    {
        $file = UploadedFile::getInstance($this, 'file1');

        if ($file) {
            $basePath = Yii::getAlias('@uploadPath/logo/');
            $fileName = md5(Yii::$app->security->generateRandomString(8)) . '.' . $file->extension;
            if (!file_exists($basePath)) {
                FileHelper::createDirectory($basePath, 7777);
            }
            $file->saveAs($basePath . $fileName);

            $this->img1 = $fileName;
        }

        if ($this->delete_file1) {
            $this->img1 = null;
        }
    }
}