<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "audio".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $type
 * @property string|null $upload_path
 */
class Audio extends \yii\db\ActiveRecord
{
    const TYPE_MAIN_AMBIENT = 1;
    const TYPE_KIDS_AMBIENT = 2;
    const TYPE_KIDS_MAIN = 3;
    const TYPE_KIDS_VIDEO = 4;
    const TYPE_MAIN_VIDEO = 5;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'audio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['title', 'upload_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'type' => 'Предназначение',
            'upload_path' => 'Ссылка',
        ];
    }

    public static function getList()
    {
        return [
            self::TYPE_MAIN_AMBIENT => 'Фоновое аудио для взрослого сайта',
            self::TYPE_KIDS_AMBIENT => 'Фоновое аудио для детского сайта',
            self::TYPE_KIDS_MAIN => 'Аудиогид для главной детского сайта',
            self::TYPE_KIDS_VIDEO => 'Аудиогид для видео детского сайта',
            self::TYPE_MAIN_VIDEO => 'Аудиогид для видео взрослого сайта',
        ];
    }
}
