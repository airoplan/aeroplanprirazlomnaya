<?php


namespace app\helpers;


use app\models\Audio;
use app\models\Logo;
use app\models\Video;

class UploadVideoHelper
{
    public static function getUsefulUrlFromVideo(Video $video)
    {
        $path = \Yii::getAlias('@uploadPath/video360/view/' . $video->upload_path);
        $path = self::getUsefulPathRecursive($path) . '/';

        return '/' . str_replace(\Yii::getAlias('@uploadPath'), 'uploads', $path);
    }

    public static function getUsefulUrlFromAudioMain(Video $video)
    {
        $path = \Yii::getAlias('@uploadPath/audio_main/' . $video->audio_main);

        return '/' . str_replace(\Yii::getAlias('@uploadPath'), 'uploads', $path);
    }

    public static function getUsefulUrlFromAudioKids(Video $video)
    {
        $path = \Yii::getAlias('@uploadPath/audio_kids/' . $video->audio_kids);

        return '/' . str_replace(\Yii::getAlias('@uploadPath'), 'uploads', $path);
    }

    public static function getUsefulUrlFromAudioBase(Audio $audio)
    {
        $path = \Yii::getAlias('@uploadPath/audio_main/' . $audio->upload_path);

        return '/' . str_replace(\Yii::getAlias('@uploadPath'), 'uploads', $path);
    }

    public static function getUsefulUrlFromLogo(Logo $logo)
    {
        $path = \Yii::getAlias('@uploadPath/logo/' . $logo->img);

        return '/' . str_replace(\Yii::getAlias('@uploadPath'), 'uploads', $path);
    }

    public static function getUsefulUrlFromLogo1(Logo $logo)
    {
        $path = \Yii::getAlias('@uploadPath/logo/' . $logo->img1);

        return '/' . str_replace(\Yii::getAlias('@uploadPath'), 'uploads', $path);
    }

    public static function getUsefulPathRecursive($path)
    {
        $files = scandir($path);
        if (in_array('index.htm', $files)) {
            return $path;
        } else {
            foreach ($files as $file) {
                if ($file !== '.' && $file !== '..' && is_dir($path . '/' . $file)) {
                    return self::getUsefulPathRecursive($path . '/' . $file);
                }
            }
        }
    }
}