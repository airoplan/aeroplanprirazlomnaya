<?php

namespace app\helpers;

class PermissionHelper
{
    const ROLE_ADMIN = 'admin';
    const ROLE_EDITOR = 'editor';

    public static function getList(): array
    {
        return [
            self::ROLE_ADMIN => 'Админ',
            self::ROLE_EDITOR => 'Редактор',
        ];
    }

}