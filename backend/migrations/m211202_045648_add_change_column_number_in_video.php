<?php

use yii\db\Migration;

/**
 * Class m211202_045648_add_change_column_number_in_video
 */
class m211202_045648_add_change_column_number_in_video extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('video', 'number');
        $this->addColumn('video', 'numbers', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('video', 'numbers');
        $this->addColumn('video', 'number', $this->integer());
    }
}
