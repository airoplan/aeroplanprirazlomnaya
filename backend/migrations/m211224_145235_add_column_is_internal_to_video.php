<?php

use yii\db\Migration;

/**
 * Class m211224_145235_add_column_is_internal_to_video
 */
class m211224_145235_add_column_is_internal_to_video extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('video', 'is_internal', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('video', 'is_internal');
    }
}
