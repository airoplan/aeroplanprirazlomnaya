<?php

use yii\db\Migration;

/**
 * Class m211023_113505_add_tbl_video
 */
class m211023_113505_add_tbl_video extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('video', [
            'id' => $this->primaryKey(),
            'active' => $this->boolean()->comment('Отображать на модели')->defaultValue(true),
            'number' => $this->integer()->comment('Номер видео'),
            'title' => $this->string()->comment('Наименование'),
            'upload_path' => $this->string(),
        ]);
        $this->createIndex('IDX_video_active', 'video', 'active');
        $this->createIndex('IDX_video_number', 'video', 'number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('video');
    }
}
