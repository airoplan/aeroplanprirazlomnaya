<?php

use yii\db\Migration;

/**
 * Class m211225_163932_add_column_to_logo
 */
class m211225_163932_add_column_to_logo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('logo', 'img1', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('logo', 'img1');
    }
}
