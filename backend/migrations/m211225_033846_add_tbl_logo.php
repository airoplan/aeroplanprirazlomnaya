<?php

use yii\db\Migration;

/**
 * Class m211225_033846_add_tbl_logo
 */
class m211225_033846_add_tbl_logo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('logo', [
            'id' => $this->primaryKey(),
            'alt' => $this->string(),
            'img' => $this->string(),
            'url' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('logo');
    }
}
