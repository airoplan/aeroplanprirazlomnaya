<?php

use yii\db\Migration;

/**
 * Class m211209_152907_add_columns_to_video
 */
class m211209_152907_add_columns_to_video extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('video', 'audio_main', $this->string());
        $this->addColumn('video', 'audio_kids', $this->string());
        $this->addColumn('video', 'text_main', $this->text());
        $this->addColumn('video', 'text_kids', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('video', 'audio_main');
        $this->dropColumn('video', 'audio_kids');
        $this->dropColumn('video', 'text_main');
        $this->dropColumn('video', 'text_kids');
    }
}
