<?php

use yii\db\Migration;

/**
 * Class m211225_020134_add_tbl_audios
 */
class m211225_020134_add_tbl_audios extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('audio', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'type' => $this->tinyInteger(),
            'upload_path' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('audio');
    }
}
