<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'frontUrl' => 'http://prirazlomnaya-360.ru/',
    'user.passwordResetTokenExpire' => 3600,
    'bsVersion' => '4.x',
    'user.passwordMinLength' => 8,
];
