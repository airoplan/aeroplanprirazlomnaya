<?php

namespace app\controllers;

use app\helpers\UploadVideoHelper;
use app\models\Audio;
use app\models\Logo;
use app\models\Video;
use yii\web\Controller;
use yii\web\Response;

class ApiController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->response->format = Response::FORMAT_JSON;
    }

    public function actionGetVideo()
    {
        $result = [];

        $vidoes = Video::find()->where(['active' => true])->all();
        foreach ($vidoes as $video) {
            $entry = [
                'title' => $video->title,
                'text_main' => $video->text_main,
                'text_kids' => $video->text_kids,
                'audio_main' => UploadVideoHelper::getUsefulUrlFromAudioMain($video),
                'audio_kids' => UploadVideoHelper::getUsefulUrlFromAudioKids($video),
                'url' => UploadVideoHelper::getUsefulUrlFromVideo($video),
                'children' => [],
                'is_internal' => (boolean) $video->is_internal,
            ];

            foreach ($video->numbers as $number) {
                $entry['children'][] = [
                    'number' => $number['video-number'] ?? null,
                    'name' => $number['name'] ?? null,
                    'url' => UploadVideoHelper::getUsefulUrlFromVideo($video) . 'index.htm?media-index=' . ($number['media-index'] ?? null),
                ];
            }

            $result[] = $entry;
        }

        return $result;
    }

    public function actionGetAudio()
    {
        $result = [
            'type_list' => Audio::getList(),
            'data' => [],
        ];

        $audios = Audio::find()->all();
        foreach ($audios as $audio) {
            $entry = [
                'title' => $audio->title,
                'type' => $audio->type,
                'url' => UploadVideoHelper::getUsefulUrlFromAudioBase($audio),
            ];

            $result['data'][] = $entry;
        }

        return $result;
    }

    public function actionGetLogo()
    {
        $result = [];

        $logos = Logo::find()->all();
        foreach ($logos as $logo) {
            $entry = [
                'alt' => $logo->alt,
                'url' => $logo->url,
                'src_colored' => UploadVideoHelper::getUsefulUrlFromLogo($logo),
                'src_white' => UploadVideoHelper::getUsefulUrlFromLogo1($logo),
            ];

            $result[] = $entry;
        }

        return $result;
    }
}